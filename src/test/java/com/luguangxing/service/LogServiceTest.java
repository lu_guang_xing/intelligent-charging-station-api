package com.luguangxing.service;

import com.luguangxing.entity.CostRecord;
import com.luguangxing.entity.GroupByMonthEntity;
import com.luguangxing.entity.LogEntity;
import com.luguangxing.utils.CommUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@SpringBootTest
class LogServiceTest {
    @Autowired
    private LogService logService;

    @Test
    void findAll() {
        List<LogEntity> logs = logService.findAll();
        if (logs != null) {
            for (LogEntity logg :
                    logs) {
                log.info(logg.toString());
            }
        }
    }

    @Test
    void findByCardId() {
        LogEntity logg = logService.findByCardId("123");
        log.info(logg.toString());
    }

    @Test
    void updateLog() {
        log.info("" + logService.updateLog("123", (float) 1.02, CommUtils.getCurrentTime()));
        findAll();
    }

    @Test
    void groupByMonth() {
        List<GroupByMonthEntity> list = logService.groupByMonth("whiteone");
        for (GroupByMonthEntity montn :
                list) {
            log.info("value" + montn.toString());
        }
    }

    @Test
    void findDailyCostByUserAccount() {
        Map<String, Object> map = new HashMap();
        map.put("userAccount", "whiteone");
        map.put("date", "2022-11-22");
        List<CostRecord> list = logService.findDailyCostByUserAccount(map);
        for (CostRecord costRecord : list){
            log.info(costRecord.toString());
        }
    }

    @Test
    void findMonthCostByUserAccount() {
        Map<String, Object> map = new HashMap();
        map.put("userAccount", "whiteone");
        map.put("date", "2022-11");
        List<CostRecord> list = logService.findMonthCostByUserAccount(map);
        for (CostRecord costRecord : list){
            log.info(costRecord.toString());
        }
    }
}