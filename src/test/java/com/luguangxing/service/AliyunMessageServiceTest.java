package com.luguangxing.service;

import com.luguangxing.utils.CommUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
class AliyunMessageServiceTest {
    @Autowired
    private AliyunMessageService aliyunMessageService;

    @Test
    void sendVeryCode() throws Exception {
        String verifyCode = CommUtils.randomVeryCode();
        log.info("your code is :" + verifyCode);
        aliyunMessageService.sendVerifyCode("17758540834", verifyCode);
        CommUtils.VeryCodeMap.put("17758540834", verifyCode);
        log.info("Map:<17758540834," + CommUtils.VeryCodeMap.get("17758540834") + ">");
    }

    @Test
    void time() throws Exception {
        log.info("time" + CommUtils.getCurrentTime());
    }
}