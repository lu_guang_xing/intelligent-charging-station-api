package com.luguangxing.service;

import com.ctg.ag.sdk.biz.AepDeviceCommandClient;
import com.ctg.ag.sdk.biz.aep_device_command.CreateCommandRequest;
import com.luguangxing.entity.UserEntity;
import com.luguangxing.utils.CommUtils;
import com.luguangxing.utils.MqttUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@SpringBootTest
class UserServiceTest {
    @Resource
    private UserService userService;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void findAll() {
        List<UserEntity> users = userService.findAll();
        if (users != null) {
            for (UserEntity user : users) {
                log.info("UserName:" + user.getUserName());
            }
        }
    }

    @Test
    void findByAnyCondition() throws Exception {

        AepDeviceCommandClient client = AepDeviceCommandClient.newClient()
                .appKey("FDi2fk7VtN5").appSecret("5MBLbglA1B")
                .build();

        CreateCommandRequest request = new CreateCommandRequest();
        // set your request params here
        request.setParamMasterKey("c21072db7c9f468089b46641134e8601");    // single value
        request.setBody("{\"content\":{\"params\":{\"ledon\":0},\"serviceIdentifier\":\"led\"},\"deviceId\":\"a0ff8dc91d0a489d8804ba67c0d00d80\",\"operator\":\"whiteone\",\"productId\":15459221,\"ttl\":0,\"level\":1}".getBytes());    //具体格式见前面请求body说明
        System.out.println("Here" + client.CreateCommand(request));

        // more requests

        client.shutdown();
    }

    @Test
    void updateBalance() {
        UserEntity user = userService.findByUserEmail("1175278814@qq.com");
        log.info("userAccount:" + user.getUserAccount());
        float newBalance = (float) 98.04;
        log.info("affect rows:" + userService.updateBalance(user.getUserCardId(), newBalance));
        user = userService.findByUserAccount(user.getUserAccount());
        log.info("" + user.getUserName() + "'s new balance:" + user.getUserCardBalance());
    }

    @Test
    void findByUserCardId() {
        UserEntity user = userService.findByUserCardId("2091701243");
        if (user != null) {
            log.info(user.toString());
        }
    }

    @Test
    void insert() {
        UserEntity user = new UserEntity("1123", "123456", "小白二号", 0, "男", "123", 0, "1175278814@qq.com", "234");
        userService.insert(user);
    }

    @Test
    void findByUserPhone() {
        UserEntity user = userService.findByUserPhone("17758540834");
        log.info(user.toString());
    }

    @Test
    void findByUserEmail() {
        UserEntity user = userService.findByUserEmail("1175278814@qq.com");
        log.info(user.toString());
    }

    @Test
    void updateUserProfile() {
        UserEntity user = userService.findByUserEmail("1175278814@qq.com");
        if (user != null) {
            log.info("user's password:" + user.getUserPassword());
            user.setUserPassword("123456");
            if (userService.updateUserProfile(user) > 0) {
                log.info("user's password:" + user.getUserPassword());
            }
        }
    }

    @Test
    void mqttTest() {
        String json = "{\"upPacketSN\":-1,\"upDataSN\":-1,\"topic\":\"v1/up/ad19\",\"timestamp\":1676988993997,\"tenantId\":\"2000144846\",\"serviceId\":8,\"protocol\":\"lwm2m\",\"productId\":\"15460104\",\"payload\":{\"Temperature_Data\":22},\"messageType\":\"dataReport\",\"deviceType\":\"\",\"deviceId\":\"0bfd07840a534b9da4f070ec2ed1be83\",\"assocAssetId\":\"\",\"IMSI\":\"undefined\",\"IMEI\":\"864160062042921\"}";

    }

}
