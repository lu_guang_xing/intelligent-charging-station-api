package com.luguangxing.service;

import com.aliyun.tea.TeaException;
import com.luguangxing.entity.UserEntity;
import com.luguangxing.utils.CommUtils;
import com.luguangxing.utils.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@Slf4j
@SpringBootTest
class ConSumeServiceTest {
    @Autowired
    private ConsumeService consumer;
    @Autowired
    private UserService userService;
    @Autowired
    private JunkMailService junkMailService;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void userWaters() {
        UserEntity user = userService.findByUserCardId("202009517111");
        log.info("修改前：" + user.toString());
        if (consumer.useWater("202009517111", 10)) {
            log.info("成功");
        }
        user = userService.findByUserCardId("202009517111");
        log.info("修改后：" + user.toString());
    }

    @Test
    void MessageTest() throws Exception {
        //java.util.List<String> args = java.util.Arrays.asList(args_);
        // 初始化 Client，采用 AK&SK 鉴权访问的方式，此方式可能会存在泄漏风险，建议使用 STS 方式。鉴权访问方式请参考：https://help.aliyun.com/document_detail/378657.html
        // 获取 AK 链接：https://usercenter.console.aliyun.com
        String verifyCode = CommUtils.randomVeryCode();
        com.aliyun.dysmsapi20170525.Client client = CommUtils.createClient("LTAI5tQ9vZEY9wqWBkEyszvX", "c8B4ehk9mOHhFib0wJ3EVPqyMxqX7w");
        com.aliyun.dysmsapi20170525.models.SendSmsRequest sendSmsRequest = new com.aliyun.dysmsapi20170525.models.SendSmsRequest()
                .setSignName("阿里云短信测试")
                .setTemplateCode("SMS_154950909")
                .setPhoneNumbers("17758540834")
                .setTemplateParam("{\"code\":\"1234\"}");
        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
        try {
            // 复制代码运行请自行打印 API 的返回值
            client.sendSmsWithOptions(sendSmsRequest, runtime);
        } catch (TeaException error) {
            // 如有需要，请打印 error
            com.aliyun.teautil.Common.assertAsString(error.message);
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 如有需要，请打印 error
            com.aliyun.teautil.Common.assertAsString(error.message);
        }
    }

    @Test
    void VeryCode() {
        String verifyCode = CommUtils.randomVeryCode();
        log.info("your code is :" + verifyCode);
        junkMailService.sendVerifyCode("1175278814@qq.com", verifyCode);
        CommUtils.VeryCodeMap.put("1175278814@qq.com", verifyCode);
        log.info("Map:<1175278814@qq.com," + CommUtils.VeryCodeMap.get("1175278814@qq.com") + ">");
    }

    @Test
    void JWTTest() {
        String user = "admin";
        String token = JWTUtil.generateToken(user);
        log.info("token:" + token);
        log.info("user:" + JWTUtil.getSubject(token));
        log.info("isExpire:" + JWTUtil.isTokenExpired(token));
    }
}