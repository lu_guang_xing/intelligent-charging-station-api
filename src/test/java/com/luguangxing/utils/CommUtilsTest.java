package com.luguangxing.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class CommUtilsTest {
    @Test
    void getMinuteDiff() {
        String startTime = "2023-06-19 04:11:27";
        int minuteDiff = CommUtils.getMinuteDiff(startTime);
        log.info("minuteDiff: {}", minuteDiff);
    }
}