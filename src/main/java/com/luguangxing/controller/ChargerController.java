package com.luguangxing.controller;

import com.luguangxing.entity.ChargeLog;
import com.luguangxing.entity.StationEntity;
import com.luguangxing.entity.StatusEntity;
import com.luguangxing.service.ChargerService;
import com.luguangxing.service.LogService;
import com.luguangxing.utils.CommUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: 小白一号
 * @date: 20/5/2023
 * @desc:
 */
@Slf4j
@RestController
@RequestMapping("/charger")
public class ChargerController {
    @Resource
    private ChargerService chargerService;
    @Resource
    private LogService logService;

    @RequestMapping(value = "/findAll", method = RequestMethod.POST)
    public Object findAll() {
        StatusEntity response = new StatusEntity();
        List<StationEntity> list = chargerService.findAll();
        if (list == null) {
            response.setStatus("Error");
            response.setCode("302");
            response.setMsg("ChargerStations is Empty");
            return response;
        } else {
            response.setStatus("success");
            response.setCode("200");
            response.setMsg("Success");
            response.setData(list);
        }
        return response;
    }

    @RequestMapping(value = "/findAllWhereUsing", method = RequestMethod.POST)
    public Object findAllWhereUsing() {
        Map<String, Object> map = new HashMap<>();
        List<ChargeLog> list = logService.findAllWhereUsing();
        if (list == null) {
            map.put("status", "Error");
            map.put("code", "302");
            map.put("msg", "ChargerStations is Empty");
            return map;
        } else {
            map.put("status", "success");
            map.put("code", "200");
            map.put("msg", "Success");
            for (ChargeLog chargeLog : list) {
                int miniute = CommUtils.getMinuteDiff(chargeLog.getEndDate());
                map.put("port" + chargeLog.getInterfaceNum(), ""+miniute);
            }
        }
        return map;
    }
}
