package com.luguangxing.controller;

import com.luguangxing.entity.StatusEntity;
import com.luguangxing.entity.UserEntity;
import com.luguangxing.service.ConsumeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: 小白一号
 * @date: 2022/11/18
 * @desc:
 */
@Slf4j
@RestController
@RequestMapping("/cost")
public class CostController {
    @Autowired
    private ConsumeService consumer;

    @GetMapping("/useWater")
    public Object userWater(@Param("userCardId") String userCardId, @Param("seconds") int seconds) {
        if (consumer.useWater(userCardId, seconds)) {
            return new StatusEntity("success", "200");
        }
        return new StatusEntity("Error", "302");
    }

    /**
     * userCardId
     */
    @PostMapping("/charge")
    public Object charge(@RequestBody Map<String, Object> map) {
        String userCardId = map.get("userCardId").toString();
        float cost = Float.parseFloat(map.get("cost").toString());
        int hour = Integer.parseInt(map.get("hour").toString());
        int interfaceNum = Integer.parseInt(map.get("interfaceNum").toString());
        if (consumer.charge(userCardId, cost, hour, interfaceNum)) {
            return new StatusEntity("success", "200");
        }
        return null;
    }
}
