package com.luguangxing.controller;

import com.luguangxing.entity.CostRecord;
import com.luguangxing.entity.GroupByMonthEntity;
import com.luguangxing.entity.StatusEntity;
import com.luguangxing.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author: 小白一号
 * @date: 2022/11/22
 * @desc:
 */
@Slf4j
@RestController
@RequestMapping("/log")
public class LogController {
    @Autowired
    private LogService logService;

    /**
     * 参数：userAccount
     *
     * @param map
     * @return
     */
    @PostMapping("/groupByMonth")
    public Object groupByMonth(@RequestBody Map<String, Object> map) {
        StatusEntity status = new StatusEntity("error", "302");
        try {
            String userAccount = map.get("userAccount").toString();
            List<GroupByMonthEntity> list = logService.groupByMonth(userAccount);
            if (list != null) {
                status.setStatus("success");
                status.setCode("200");
                status.setData(list);
            }
        } catch (Exception e) {

        }
        return status;
    }

    /**
     * userAccount,date
     *
     * @param map
     * @return
     */
    @PostMapping("/findDailyCostByUserAccount")
    public Object findDailyCostByUserAccount(@RequestBody Map<String, Object> map) {
        StatusEntity status = new StatusEntity("error", "302");
        try {
            List<CostRecord> list = logService.findDailyCostByUserAccount(map);
            if (list != null) {
                status.setStatus("success");
                status.setCode("200");
                status.setData(list);
            }
        } catch (Exception e) {

        }
        return status;
    }

    /**
     * userAccount,date
     *
     * @param map
     * @return
     */
    @PostMapping("/findMonthCostByUserAccount")
    public Object findMonthCostByUserAccount(@RequestBody Map<String, Object> map) {
        StatusEntity status = new StatusEntity("error", "302");
        try {
            List<CostRecord> list = logService.findMonthCostByUserAccount(map);
            if (list != null) {
                status.setStatus("success");
                status.setCode("200");
                status.setData(list);
            }
        } catch (Exception e) {

        }
        return status;
    }
}
