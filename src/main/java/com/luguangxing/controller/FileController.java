package com.luguangxing.controller;

import cn.hutool.extra.servlet.ServletUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Map;

/**
 * @author: 小白一号
 * @date: 11/5/2023
 * @desc:
 */

@Slf4j
@RestController
@RequestMapping("/file")
public class FileController {

    @GetMapping("/download")
    @ResponseBody()
    public void downloadHutool(@RequestParam(value = "fileName") String fileName, HttpServletResponse response) {
        log.info("收到下载请求！\n");
        response.setCharacterEncoding("UTF-8");
        ServletUtil.write(response, new File("Files/" + fileName));
    }
}
