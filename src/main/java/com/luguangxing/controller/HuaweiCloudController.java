package com.luguangxing.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author: 小白一号
 * @date: 7/6/2023
 * @desc:
 */
@RestController
@Slf4j
@RequestMapping("/huaweiCloud")
public class HuaweiCloudController {
    /**
     * @param map
     * @return
     * @描述： 测试华为云数据转发规则
     * @测试结果： 连通性正常，可以收到数据
     */
    @PostMapping("/upload")
    public Object upload(@RequestBody Map<String, Object> map) {
        //分割线
        log.info(" -------------------------------------------");
        log.info("收到上传请求！");
        log.info("收到的参数为：" + map.toString());
        log.info(" -------------------------------------------");
        return null;
    }
}
