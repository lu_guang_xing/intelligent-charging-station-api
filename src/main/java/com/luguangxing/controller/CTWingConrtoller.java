package com.luguangxing.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ctg.ag.sdk.biz.AepDeviceCommandClient;
import com.ctg.ag.sdk.biz.AepDeviceStatusClient;
import com.ctg.ag.sdk.biz.AepProductManagementClient;
import com.ctg.ag.sdk.biz.aep_device_command.CreateCommandRequest;
import com.ctg.ag.sdk.biz.aep_device_command.QueryCommandListRequest;
import com.ctg.ag.sdk.biz.aep_device_status.QueryDeviceStatusRequest;
import com.ctg.ag.sdk.biz.aep_device_status.QueryDeviceStatusResponse;
import com.ctg.ag.sdk.biz.aep_product_management.QueryProductListRequest;
import com.ctg.ag.sdk.biz.aep_product_management.QueryProductListResponse;
import com.luguangxing.entity.CostRecord;
import com.luguangxing.entity.StatusEntity;
import com.luguangxing.entity.UserEntity;
import com.luguangxing.service.UploadedDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: 小白一号
 * @date: 2023/2/20
 * @desc:
 */
@Slf4j
@RestController
@RequestMapping("/CTWing")
public class CTWingConrtoller {

    @Resource
    private UploadedDataService uploadedDataService;

    //    批量查询产品信息
    @GetMapping("/QueryProductList")
    public Object queryProductList() throws Exception {
        AepProductManagementClient client = AepProductManagementClient.newClient()
                .appKey("FDi2fk7VtN5").appSecret("5MBLbglA1B")
                .build();
        QueryProductListRequest request = new QueryProductListRequest();
        request.setParamSearchValue(null);    // single value
        request.setParamPageNow(null);    // single value
        request.setParamPageSize(null);    // single value
        QueryProductListResponse response = client.QueryProductList(request);
        log.info(response.toString());

        return new String(response.getBody());
    }

    //开关灯
    @PostMapping("/Controlled")
    public UserEntity controlled(@RequestBody Map<String, Object> map) throws Exception {
        String body = "";
        if (map.get("is_allow").equals("AAA")) {
            body = "{\"content\":{\"params\":{\"end\":\"AAA\"},\"serviceIdentifier\":\"is_allow\"},\"deviceId\":\"a0ff8dc91d0a489d8804ba67c0d00d80\",\"operator\":\"whiteone\",\"productId\":15459221,\"ttl\":0,\"level\":1}";
        } else if (map.get("is_allow").equals("BBB")) {
            body = "{\"content\":{\"params\":{\"end\":\"BBB\"},\"serviceIdentifier\":\"is_allow\"},\"deviceId\":\"a0ff8dc91d0a489d8804ba67c0d00d80\",\"operator\":\"whiteone\",\"productId\":15459221,\"ttl\":0,\"level\":1}";
        }
        AepDeviceCommandClient client = AepDeviceCommandClient.newClient()
                .appKey("FDi2fk7VtN5").appSecret("5MBLbglA1B")
                .build();

        CreateCommandRequest request = new CreateCommandRequest();
        // set your request params here
        request.setParamMasterKey("c21072db7c9f468089b46641134e8601");    // single value
        Map<String, Object> bodyMap = new HashMap();
        // "{\"content\":{\"params\":{\"ledon\":0},\"serviceIdentifier\":\"led\"},\"deviceId\":\"a0ff8dc91d0a489d8804ba67c0d00d80\",\"operator\":\"whiteone\",\"productId\":15459221,\"ttl\":0,\"level\":1}"
        request.setBody(body.getBytes());    //具体格式见前面请求body说明
        System.out.println("Here" + client.CreateCommand(request));
        // more requests

        client.shutdown();

        return null;
    }

    //查询当前设备传感器值
    @PostMapping("/QueryDeviceStatus")
    public Object queryDevicesStatus(@RequestBody Map<String, Object> map) throws Exception {
        AepDeviceStatusClient client = AepDeviceStatusClient.newClient()
                .appKey("FDi2fk7VtN5").appSecret("5MBLbglA1B")
                .build();
        String body = "{\"productId\":\"15459221\",\"deviceId\":\"a0ff8dc91d0a489d8804ba67c0d00d80\",\"datasetId\":\"" + map.get("payload").toString() + "\"}";
        QueryDeviceStatusRequest request = new QueryDeviceStatusRequest();
        // set your request params here
        request.setBody(body.getBytes());    //具体格式见前面请求body说明
//        map.clear();
        QueryDeviceStatusResponse response = client.QueryDeviceStatus(request);
//        JSONObject jsonObject = JSON.parseObject(response);
        log.info(response.toString());
        client.shutdown();

//        return jsonObject.get("body").toString();
        return new String(response.getBody());
    }

    //分页查询 limit
    @PostMapping("/findWithLimit")
    public Object findWithLimit(@RequestBody Map<String, Object> map) throws Exception {
        return uploadedDataService.findWithLimit(map);
    }

    /**
     * userAccount,date
     *
     * @param map
     * @return
     */
    @PostMapping("/findDailyCostByUserAccount")
    public Object findDailyCostByUserAccount(@RequestBody Map<String, Object> map) {
        StatusEntity status = new StatusEntity("error", "302");
        try {
            List<CostRecord> list = uploadedDataService.findDailyCostByUserAccount(map);
            if (list != null) {
                status.setStatus("success");
                status.setCode("200");
                status.setData(list);
            } else {
                status.setStatus("list is null");
                status.setCode("302");
            }
        } catch (Exception e) {

        }
        return status;
    }

    /**
     * userAccount,date
     *
     * @param map
     * @return
     */
    @PostMapping("/findMonthCostByUserAccount")
    public Object findMonthCostByUserAccount(@RequestBody Map<String, Object> map) {
        StatusEntity status = new StatusEntity("error", "302");
        try {
            List<CostRecord> list = uploadedDataService.findMonthCostByUserAccount(map);
            if (list != null) {
                status.setStatus("success");
                status.setCode("200");
                status.setData(list);
            } else {
                status.setStatus("list is null");
                status.setCode("302");
            }
        } catch (Exception e) {

        }
        return status;
    }

    /**
     * userAccount,date
     *
     * @param map date
     * @return
     */
    @PostMapping("/findDailyCostCommandResponse")
    public Object findDailyCostCommandResponse(@RequestBody Map<String, Object> map) throws Exception {
        AepDeviceCommandClient client = AepDeviceCommandClient.newClient()
                .appKey("FDi2fk7VtN5").appSecret("5MBLbglA1B")
                .build();
        QueryCommandListRequest request = new QueryCommandListRequest();
        // set your request params here
        request.setParamMasterKey("c21072db7c9f468089b46641134e8601");    // single value
        request.setParamProductId(15459221);    // single value
        request.setParamDeviceId("a0ff8dc91d0a489d8804ba67c0d00d80");    // single value
        request.setParamStartTime(map.get("startTime").toString());    // single value
        request.setParamEndTime(map.get("endTime").toString());    // single value
        request.setParamPageNow(null);    // single value
        request.setParamPageSize(20);    // single value
        System.out.println(client.QueryCommandList(request));
        String responseBody = new String(client.QueryCommandList(request).getBody());
        JSONObject jsonObject = JSON.parseObject(responseBody);
        jsonObject = JSON.parseObject(jsonObject.get("result").toString());
        List<Map<String, Object>> list = (List<Map<String, Object>>) jsonObject.get("list");
        for (Map<String, Object> per :
                list) {
            per.put("finishTime", per.get("finishTime").toString());
        }

        client.shutdown();
        StatusEntity status = new StatusEntity("error", "302");
        try {
            status.setStatus("success");
            status.setCode("200");
            status.setData(jsonObject.get("list"));
        } catch (Exception e) {

        }
        return status;
    }

    /**
     * userAccount,date
     *
     * @param map
     * @return
     */
    @PostMapping("/findMonthCostCommandResponse")
    public Object findMonthCostCommandResponse(@RequestBody Map<String, Object> map) throws Exception {
        AepDeviceCommandClient client = AepDeviceCommandClient.newClient()
                .appKey("FDi2fk7VtN5").appSecret("5MBLbglA1B")
                .build();

        QueryCommandListRequest request = new QueryCommandListRequest();
        // set your request params here
        request.setParamMasterKey("c21072db7c9f468089b46641134e8601");    // single value
        request.setParamProductId(15459221);    // single value
        request.setParamDeviceId("a0ff8dc91d0a489d8804ba67c0d00d80");    // single value
        request.setParamStartTime(map.get("startTime").toString());    // single value
        request.setParamEndTime(map.get("endTime").toString());    // single value
        request.setParamPageNow(null);    // single value
        request.setParamPageSize(20);    // single value
        System.out.println(client.QueryCommandList(request));
        String responseBody = new String(client.QueryCommandList(request).getBody());
        JSONObject jsonObject = JSON.parseObject(responseBody);
        jsonObject = JSON.parseObject(jsonObject.get("result").toString());
        List<Map<String, Object>> list = (List<Map<String, Object>>) jsonObject.get("list");
        for (Map<String, Object> per :
                list) {
            per.put("finishTime", per.get("finishTime").toString());
        }
        // more requests

        client.shutdown();
        StatusEntity status = new StatusEntity("error", "302");
        try {
            status.setStatus("success");
            status.setCode("200");
            status.setData(jsonObject.get("list"));
        } catch (Exception e) {

        }
        return status;
    }

}
