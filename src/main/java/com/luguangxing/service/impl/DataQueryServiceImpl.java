package com.luguangxing.service.impl;

import com.ctg.ag.sdk.biz.AepDeviceCommandClient;
import com.ctg.ag.sdk.biz.aep_device_command.CreateCommandRequest;
import com.luguangxing.service.DataQueryService;
import com.luguangxing.utils.Global;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: 小白一号
 * @date: 2023/2/20
 * @desc:
 */
@Slf4j
@Service
public class DataQueryServiceImpl implements DataQueryService {

    @Override
    public Map<String, Object> CreateCommand(Map<String, Object> map) throws Exception {
        AepDeviceCommandClient client = AepDeviceCommandClient.newClient()
                .appKey(Global.appKey).appSecret(Global.appSecret)
                .build();

        CreateCommandRequest request = new CreateCommandRequest();
        // set your request params here
        request.setParamMasterKey(Global.masterKey);    // single value
        Map<String, Object> bodyMap = new HashMap();
        // "{\"content\":{\"params\":{\"ledon\":0},\"serviceIdentifier\":\"led\"},\"deviceId\":\"a0ff8dc91d0a489d8804ba67c0d00d80\",\"operator\":\"whiteone\",\"productId\":15459221,\"ttl\":0,\"level\":1}"
        String body = "{\"content\":{\"params\":{\"ledon\":0},\"serviceIdentifier\":\"" + map.get("serviceIdentifier") + "\"},\"deviceId\":\"a0ff8dc91d0a489d8804ba67c0d00d80\",\"operator\":\"whiteone\",\"productId\":15459221,\"ttl\":0,\"level\":1}";
        request.setBody(body.getBytes());    //具体格式见前面请求body说明
        System.out.println("Here" + client.CreateCommand(request));

        // more requests

        client.shutdown();

        return null;
    }
}
