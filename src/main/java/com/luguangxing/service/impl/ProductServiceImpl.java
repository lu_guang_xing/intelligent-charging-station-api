package com.luguangxing.service.impl;

import com.luguangxing.mapper.ProductMapper;
import com.luguangxing.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author: 小白一号
 * @date: 2023/2/20
 * @desc:
 */
@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public int saveProduct(Map<String, Object> map) {
        return productMapper.saveProduct(map);
    }
}
