package com.luguangxing.service.impl;

import com.luguangxing.entity.CostRecord;
import com.luguangxing.entity.UploadedData;
import com.luguangxing.mapper.UploadedDataMapper;
import com.luguangxing.service.UploadedDataService;
import com.luguangxing.utils.CommUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author: 小白一号
 * @date: 2023/2/20
 * @desc:
 */
@Slf4j
@Service
public class UploadedDataServiceImpl implements UploadedDataService {
    @Autowired
    private UploadedDataMapper uploadedDataMapper;

    @Override
    public int saveUploadedData(Map<String, Object> map) {
        map.put("timestamp", CommUtils.toDate(Long.parseLong(map.get("timestamp").toString())));
        return uploadedDataMapper.saveUploadedData(map);
    }

    @Override
    public List<UploadedData> findWithLimit(Map<String, Object> map) {
        return uploadedDataMapper.findWithLimit(map);
    }

    @Override
    public List<CostRecord> findDailyCostByUserAccount(Map<String, Object> map) {
        return uploadedDataMapper.findDailyCostByUserAccount(map);
    }

    @Override
    public List<CostRecord> findMonthCostByUserAccount(Map<String, Object> map) {
        return uploadedDataMapper.findMonthCostByUserAccount(map);
    }
}
