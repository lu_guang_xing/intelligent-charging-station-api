package com.luguangxing.service.impl;

import com.luguangxing.entity.UserEntity;
import com.luguangxing.mapper.LogMapper;
import com.luguangxing.mapper.UserMapper;
import com.luguangxing.service.ConsumeService;
import com.luguangxing.service.LogService;
import com.luguangxing.service.UserService;
import com.luguangxing.utils.CommUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: 小白一号
 * @date: 2022/11/18
 * @desc:
 */

@Transactional  //事务注解,自动开启事务
@Service
public class ConSumeServiceImpl implements ConsumeService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private LogMapper logMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private LogService logService;
    @Value("${water_card.pricePerSecond}")
    float pricePerSecond;

    @Override
    public boolean useWater(String userCardId, int seconds) {
        UserEntity user = userService.findByUserCardId(userCardId);
        if (user == null) {
            return false;
        }
        float cost = pricePerSecond * seconds;
        if (userService.updateBalance(userCardId, user.getUserCardBalance() - cost) > 0) {
            if (logService.updateLog(user.getUserCardId(), cost, CommUtils.getCurrentTime()) > 0) {
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    public boolean charge(String userCardId, float cost, int hour, int interfaceNum) {
        UserEntity user = userService.findByUserCardId(userCardId);
        if (user == null) {
            return false;
        }
        float newBalance = user.getUserCardBalance() - cost;
        if (userService.updateBalance(userCardId, newBalance) > 0) {
            if (logService.updateChargeLog(user.getUserCardId(), cost, hour, interfaceNum) > 0) {
                return true;
            }
            return false;
        }
        return false;
    }
}
