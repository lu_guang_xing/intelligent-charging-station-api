package com.luguangxing.service.impl;

import com.luguangxing.entity.UserEntity;
import com.luguangxing.mapper.UserMapper;
import com.luguangxing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: 小白一号
 * @date: 2022/11/17
 * @desc:
 */
@Transactional  //事务注解,自动开启事务
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<UserEntity> findAll() {
        return userMapper.findAll();
    }

    @Override
    public UserEntity findByUserAccount(String userAccount) {
        Map<String, Object> map = new HashMap();
        map.put("userAccount", userAccount);
        return userMapper.findByAnyCondition(map);
    }

    @Override
    public UserEntity findByUserPhone(String userPhone) {
        Map<String, Object> map = new HashMap();
        map.put("userPhone", userPhone);
        return userMapper.findByAnyCondition(map);
    }

    @Override
    public UserEntity findByUserEmail(String userEmail) {
        Map<String, Object> map = new HashMap();
        map.put("userEmail", userEmail);
        return userMapper.findByAnyCondition(map);
    }

    @Override
    public UserEntity findByUserCardId(String userCardId) {
        if (userCardId != null && userCardId != "") {
            Map<String, Object> map = new HashMap();
            map.put("userCardId", userCardId);
            return userMapper.findByAnyCondition(map);
        }
        return null;
    }

    @Override
    public UserEntity findByAnyCondition(Map<String, Object> map) {
        return userMapper.findByAnyCondition(map);
    }

    @Override
    public int updateBalance(String userCardId, float newBalance) {
        Map<String, Object> map = new HashMap();
        map.put("userCardId", userCardId);
        map.put("newBalance", newBalance);
        return userMapper.updateBalance(map);
    }

    @Override
    public int insert(UserEntity user) {
        return userMapper.insert(user);
    }

    @Override
    public int updateUserProfile(UserEntity user) {
        return userMapper.updateUserProfile(user);
    }
}
