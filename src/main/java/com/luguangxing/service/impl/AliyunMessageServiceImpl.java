package com.luguangxing.service.impl;

import com.aliyun.tea.TeaException;
import com.luguangxing.service.AliyunMessageService;
import com.luguangxing.utils.CommUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author: 小白一号
 * @date: 2022/11/18
 * @desc:
 */
@Slf4j
@Service
public class AliyunMessageServiceImpl implements AliyunMessageService {
    @Resource
    private RedisTemplate redisTemplate;

    @Override
    public boolean sendVerifyCode(String phone, String verifyCode) throws Exception {
        com.aliyun.dysmsapi20170525.Client client = CommUtils.createClient("LTAI5tQ9vZEY9wqWBkEyszvX", "c8B4ehk9mOHhFib0wJ3EVPqyMxqX7w");
        com.aliyun.dysmsapi20170525.models.SendSmsRequest sendSmsRequest = new com.aliyun.dysmsapi20170525.models.SendSmsRequest()
                .setSignName("阿里云短信测试")
                .setTemplateCode("SMS_154950909")
                .setPhoneNumbers(phone)
                .setTemplateParam("{\"code\":" + verifyCode + "}");
        redisTemplate.opsForValue().set(phone, verifyCode, 5, TimeUnit.MINUTES);
        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
        try {
            // 复制代码运行请自行打印 API 的返回值
            log.info(client.sendSmsWithOptions(sendSmsRequest, runtime).toString());
        } catch (TeaException error) {
            // 如有需要，请打印 error
            log.info(com.aliyun.teautil.Common.assertAsString(error.message));
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 如有需要，请打印 error
            log.info(com.aliyun.teautil.Common.assertAsString(error.message));
        }
        return true;
    }
}
