package com.luguangxing.service.impl;

import com.luguangxing.service.JunkMailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;
import java.util.concurrent.TimeUnit;

/**
 * @author: 小白一号
 * @date: 2022/11/18
 * @desc:
 */
@Slf4j
@Service
public class JunkMailServiceImpl implements JunkMailService {

    @Resource
    JavaMailSender mailSender;

    @Resource
    private RedisTemplate redisTemplate;

    @Value("${spring.mail.username}")
    private String from;

    @Override
    public boolean sendVerifyCode(String emailAccount, String verifyCode) {
        try {
            MimeMessage mimeMessage = this.mailSender.createMimeMessage();
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
            message.setFrom(from);
            message.setSubject("智能充电桩APP");
            message.setTo(emailAccount);
            message.setText("当前正在使用邮箱注册--智能充电桩APP\n您的验证码为: " + verifyCode + " (5分钟有效)\n如非您本人操作，请忽略此邮件！");
            this.mailSender.send(mimeMessage);
            redisTemplate.opsForValue().set(emailAccount, verifyCode, 5, TimeUnit.MINUTES);
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
        return true;
    }
}
