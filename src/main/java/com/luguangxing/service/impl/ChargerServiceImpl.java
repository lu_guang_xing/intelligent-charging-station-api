package com.luguangxing.service.impl;

import com.luguangxing.entity.StationEntity;
import com.luguangxing.mapper.ChargerMapper;
import com.luguangxing.service.ChargerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: 小白一号
 * @date: 20/5/2023
 * @desc:
 */
@Service
public class ChargerServiceImpl implements ChargerService {
    @Resource
    private ChargerMapper chargerMapper;

    @Override
    public List<StationEntity> findAll() {
        return chargerMapper.findAll();
    }
}
