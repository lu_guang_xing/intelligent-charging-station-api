package com.luguangxing.service.impl;

import com.luguangxing.mapper.CommandResponseMapper;
import com.luguangxing.service.CommandResponseService;
import com.luguangxing.utils.CommUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author: 小白一号
 * @date: 2023/2/20
 * @desc:
 */
@Slf4j
@Service
public class CommandResponseServiceImpl implements CommandResponseService {
    @Autowired
    private CommandResponseMapper commandResponseMapper;

    @Override
    public int saveCommandResponse(Map<String, Object> map) {
        map.put("timestamp", CommUtils.toDate(Long.parseLong(map.get("timestamp").toString())));
        return commandResponseMapper.saveCommandResponse(map);
    }
}
