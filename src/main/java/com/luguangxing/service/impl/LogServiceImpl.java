package com.luguangxing.service.impl;

import com.luguangxing.entity.ChargeLog;
import com.luguangxing.entity.CostRecord;
import com.luguangxing.entity.GroupByMonthEntity;
import com.luguangxing.entity.LogEntity;
import com.luguangxing.mapper.LogMapper;
import com.luguangxing.service.LogService;
import com.luguangxing.utils.CommUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: 小白一号
 * @date: 2022/11/17
 * @desc:
 */
@Transactional  //事务注解,自动开启事务
@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogMapper logMapper;

    @Override
    public List<LogEntity> findAll() {
        return logMapper.findAll();
    }

    @Override
    public List<ChargeLog> findAllWhereUsing() {
        return logMapper.findAllWhereUsing();
    }

    @Override
    public LogEntity findByCardId(@Param("cardId") String cardId) {
        return logMapper.findByCardId(cardId);
    }

    @Override
    public int updateLog(String userCardId, float cost, String date) {
        Map<String, Object> map = new HashMap();
        map.put("userCardId", userCardId);
        map.put("cost", cost);
        map.put("logDate", date);
        return logMapper.updateLog(map);
    }

    @Override
    public int updateChargeLog(String userCardId, float cost, int hour, int interfaceNum) {
        Map<String, Object> map = new HashMap();
        map.put("userCardId", userCardId);
        map.put("cost", cost);
        map.put("startDate", CommUtils.getCurrentTime());
        map.put("endDate", CommUtils.getAfterTime(hour));
        map.put("interfaceNum", interfaceNum);
        return logMapper.updateChargeLog(map);
    }

    @Override
    public List<GroupByMonthEntity> groupByMonth(String userAccount) {
        return logMapper.groupByMonth(userAccount);
    }

    @Override
    public List<CostRecord> findDailyCostByUserAccount(Map<String, Object> map) {
        return logMapper.findDailyCostByUserAccount(map);
    }

    @Override
    public List<CostRecord> findMonthCostByUserAccount(Map<String, Object> map) {
        return logMapper.findMonthCostByUserAccount(map);
    }
}
