package com.luguangxing.service;

import com.luguangxing.entity.ChargeLog;
import com.luguangxing.entity.CostRecord;
import com.luguangxing.entity.GroupByMonthEntity;
import com.luguangxing.entity.LogEntity;

import java.util.List;
import java.util.Map;

public interface LogService {
    List<LogEntity> findAll();
    List<ChargeLog> findAllWhereUsing();

    LogEntity findByCardId(String cardId);

    int updateLog(String userCardId, float cost, String date);

    int updateChargeLog(String userCardId, float cost, int hour, int interfaceNum);

    List<GroupByMonthEntity> groupByMonth(String userAccount);

    List<CostRecord> findDailyCostByUserAccount(Map<String, Object> map);

    List<CostRecord> findMonthCostByUserAccount(Map<String, Object> map);

}
