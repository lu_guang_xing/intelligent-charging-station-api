package com.luguangxing.service;

import com.luguangxing.entity.StationEntity;

import java.util.List;

public interface ChargerService {
    List<StationEntity> findAll();
}
