package com.luguangxing.service;

public interface AliyunMessageService {
    boolean sendVerifyCode(String phone,String verifyCode) throws Exception;
}
