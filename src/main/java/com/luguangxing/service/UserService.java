package com.luguangxing.service;

import com.luguangxing.entity.UserEntity;

import java.util.List;
import java.util.Map;

public interface UserService {
    List<UserEntity> findAll();

    UserEntity findByAnyCondition(Map<String,Object> map);

    UserEntity findByUserAccount(String userAccount);

    UserEntity findByUserPhone(String userPhone);

    UserEntity findByUserEmail(String userEmail);

    UserEntity findByUserCardId(String userCardId);

    int updateBalance(String userCardId,float newBalance);

    int insert(UserEntity user);

    int updateUserProfile(UserEntity user);
}
