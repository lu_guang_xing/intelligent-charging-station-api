package com.luguangxing.service;

public interface ConsumeService {
    boolean useWater(String userCardId, int seconds);

    boolean charge(String userCardId, float cost, int hour, int interfaceNum);
}
