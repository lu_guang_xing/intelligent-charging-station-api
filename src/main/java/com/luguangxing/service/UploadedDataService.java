package com.luguangxing.service;

import com.luguangxing.entity.CostRecord;
import com.luguangxing.entity.UploadedData;

import java.util.List;
import java.util.Map;

public interface UploadedDataService {
    int saveUploadedData(Map<String, Object> map);

    List<UploadedData> findWithLimit(Map<String, Object> map);

    List<CostRecord> findDailyCostByUserAccount(Map<String, Object> map);

    List<CostRecord> findMonthCostByUserAccount(Map<String, Object> map);
}
