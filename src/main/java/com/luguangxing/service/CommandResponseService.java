package com.luguangxing.service;

import java.util.Map;

public interface CommandResponseService {
    int saveCommandResponse(Map<String, Object> map);
}
