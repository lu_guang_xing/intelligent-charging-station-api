package com.luguangxing.mapper;

import com.luguangxing.entity.LogEntity;
import com.luguangxing.entity.StationEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: 小白一号
 * @date: 20/5/2023
 * @desc:
 */

@Mapper
public interface ChargerMapper {
    List<StationEntity> findAll();
}
