package com.luguangxing.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface ProductMapper {
    int saveProduct(Map<String, Object> map);
}
