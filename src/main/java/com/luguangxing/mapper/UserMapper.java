package com.luguangxing.mapper;

import com.luguangxing.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author: 小白一号
 * @date: 2022/11/17
 * @desc:
 */
@Mapper
public interface UserMapper {
    List<UserEntity> findAll();

    UserEntity findByAnyCondition(Map<String,Object> map);

    int updateBalance(Map<String,Object> map);

    int insert(UserEntity user);

    int updateUserProfile(UserEntity user);
}
