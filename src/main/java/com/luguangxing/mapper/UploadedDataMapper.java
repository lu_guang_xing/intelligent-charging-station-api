package com.luguangxing.mapper;

import com.luguangxing.entity.CostRecord;
import com.luguangxing.entity.UploadedData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author: 小白一号
 * @date: 2023/2/20
 * @desc:
 */
@Mapper
public interface UploadedDataMapper {

    int saveUploadedData(Map<String, Object> map);

    List<UploadedData> findWithLimit(Map<String, Object> map);

    List<CostRecord> findDailyCostByUserAccount(Map<String, Object> map);

    List<CostRecord> findMonthCostByUserAccount(Map<String, Object> map);
}
