package com.luguangxing.mapper;

import com.luguangxing.entity.ChargeLog;
import com.luguangxing.entity.CostRecord;
import com.luguangxing.entity.GroupByMonthEntity;
import com.luguangxing.entity.LogEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author: 小白一号
 * @date: 2022/11/17
 * @desc:
 */
@Mapper
public interface LogMapper {
    List<LogEntity> findAll();

    List<ChargeLog> findAllWhereUsing();

    LogEntity findByCardId(String cardId);

    int updateLog(Map<String, Object> map);

    int updateChargeLog(Map<String, Object> map);

    List<GroupByMonthEntity> groupByMonth(String userAccount);

    List<CostRecord> findDailyCostByUserAccount(Map<String, Object> map);

    List<CostRecord> findMonthCostByUserAccount(Map<String, Object> map);
}