package com.luguangxing.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface CommandResponseMapper {
    int saveCommandResponse(Map<String, Object> map);
}
