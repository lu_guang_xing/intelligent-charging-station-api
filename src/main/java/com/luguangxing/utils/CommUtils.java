package com.luguangxing.utils;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author: 小白一号
 * @date: 2022/11/18
 * @desc:
 */
public class CommUtils {

    public static Map<String, String> VeryCodeMap = new HashMap();

    /**
     * 获取当前日期：yyyy-MM-dd hh:mm:ss
     *
     * @return String
     */
    public static String getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);  //Local.root表示显示时间的格式按照系统上当地时间显示，还有Local.Chinese 等
        //在1989.4.11-1989.9.17期间id为GMT+08:00比id为Asia/Shanghai毫秒值多一个小时，其余时间段相等 修改方案：时区id改为:Asia/Shanghai
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));    //可以通过此芳芳设置当前时区为东八区，如不设置，默认是当前手机系统所指时区,GMT+08:00时区在
        String formatedTimeStr = dateFormat.format(new Date());   //
        return formatedTimeStr;
    }

    /**
     * 获取当前时间加上指定小时之后的日期
     */
    public static String getAfterTime(int hour) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);  //Local.root表示显示时间的格式按照系统上当地时间显示，还有Local.Chinese 等
        //在1989.4.11-1989.9.17期间id为GMT+08:00比id为Asia/Shanghai毫秒值多一个小时，其余时间段相等 修改方案：时区id改为:Asia/Shanghai
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));    //可以通过此芳芳设置当前时区为东八区，如不设置，默认是当前手机系统所指时区,GMT+08:00时区在
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, hour);
        String formatedTimeStr = dateFormat.format(calendar.getTime());   //
        return formatedTimeStr;
    }

    /**
     * 获取当前时间和指定时间的分钟差
     */
    public static int getMinuteDiff(String time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);  //Local.root表示显示时间的格式按照系统上当地时间显示，还有Local.Chinese 等
        //在1989.4.11-1989.9.17期间id为GMT+08:00比id为Asia/Shanghai毫秒值多一个小时，其余时间段相等 修改方案：时区id改为:Asia/Shanghai
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));    //可以通过此芳芳设置当前时区为东八区，如不设置，默认是当前手机系统所指时区,GMT+08:00时区在
        try {
            Date date1 = dateFormat.parse(time);
            Date date2 = dateFormat.parse(getCurrentTime());
            long diff = date1.getTime() - date2.getTime();
            return (int) (diff / (1000 * 60));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 获取当前日期：yyyyMMdd hh:mm:ss
     *
     * @return String
     */
    public static String getCurrentTimeNoDash() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ROOT);  //Local.root表示显示时间的格式按照系统上当地时间显示，还有Local.Chinese 等
        //在1989.4.11-1989.9.17期间id为GMT+08:00比id为Asia/Shanghai毫秒值多一个小时，其余时间段相等 修改方案：时区id改为:Asia/Shanghai
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));    //可以通过此芳芳设置当前时区为东八区，如不设置，默认是当前手机系统所指时区,GMT+08:00时区在
        String formatedTimeStr = dateFormat.format(new Date());   //
        return formatedTimeStr;
    }

    /**
     * 毫秒时间戳转日期String
     */
    public static String toDate(long time) {
        //当前时间毫秒的时间戳转换为日期
        Date millisecondDate = new Date(time);
        //格式化时间
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String millisecondStrings = formatter.format(millisecondDate);
        return millisecondStrings;
    }

    /**
     * 使用AK&SK初始化账号Client
     *
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.dysmsapi20170525.Client createClient(String accessKeyId, String accessKeySecret) throws
            Exception {
        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config()
                // 必填，您的 AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 必填，您的 AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new com.aliyun.dysmsapi20170525.Client(config);
    }

    /**
     * 生成4位数随机码
     *
     * @return
     */
    public static String randomVeryCode() {
        return String.format("%06d", new Random().nextInt(1000000));

    }

    //获取当前日期 yyyy-MM-dd
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ROOT);  //Local.root表示显示时间的格式按照系统上当地时间显示，还有Local.Chinese 等
        //在1989.4.11-1989.9.17期间id为GMT+08:00比id为Asia/Shanghai毫秒值多一个小时，其余时间段相等 修改方案：时区id改为:Asia/Shanghai
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));    //可以通过此芳芳设置当前时区为东八区，如不设置，默认是当前手机系统所指时区,GMT+08:00时区在
        String formatedTimeStr = dateFormat.format(new Date());   //
        return formatedTimeStr;
    }


}
