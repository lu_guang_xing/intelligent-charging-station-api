package com.luguangxing.utils;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author: 小白一号
 * @date: 2023/2/20
 * @desc:
 */
public class Global {
    @Value("${ctwing.app_key}")
    public static String appKey;
    @Value("${ctwing.app_secret}")
    public static String appSecret;
    @Value("${ctwing.master_key}")
    public static String masterKey;
    @Value("${ctwing.device_id}")
    public static String deviceId;
    @Value("${ctwing.operator}")
    public static String operator;
    @Value("${ctwing.product_id}")
    public static String productId;
}
