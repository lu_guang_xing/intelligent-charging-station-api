package com.luguangxing.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

/**
 * @author: 小白一号
 * @date: 2022/11/18
 * @desc:
 */
@Slf4j
public class EmailUtils {
    @Resource
    JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String from;


    public boolean sendJunkMail(String emailAccount,String veryCode) {
        try {
            MimeMessage mimeMessage = this.mailSender.createMimeMessage();
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
            message.setFrom(from);
            message.setSubject("发送邮箱测试！");
            message.setTo(emailAccount);
            message.setText("Hello,QQEMail!Your code is :"+veryCode);
            this.mailSender.send(mimeMessage);
        } catch (Exception e) {
            log.error(e.toString());
            return false;
        }
        return true;
    }
}
