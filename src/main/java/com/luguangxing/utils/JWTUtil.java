package com.luguangxing.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * @author: 小白一号
 * @date: 19/5/2023
 * @desc:
 */
@Slf4j
public class JWTUtil {
    // 过期时间30分钟
    private static final long EXPIRE_TIME = 7 * 24 * 60 * 60 * 1000L;
    // Token 私钥
    private static final String SECRET_KEY = "my-secret-key";

    /**
     * 生成 Token
     *
     * @param subject 用户 ID 或者用户名
     * @return jwt token
     */
    public static String generateToken(String subject) {
        Date nowDate = new Date();
        // 过期时间
        Date expireDate = new Date(nowDate.getTime() + EXPIRE_TIME);

        return Jwts.builder()
                .setId(subject)
                .setIssuedAt(nowDate)
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();
    }

    /**
     * 解析 Token
     *
     * @param token jwt token
     * @return Claims
     */
    public static Claims parseToken(String token) {
        return Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * 获取 Token 中注册信息
     *
     * @param token jwt token
     * @return 用户 ID 或者用户名
     */
    public static String getSubject(String token) {
        return parseToken(token).getId();
    }

    /**
     * 判断 Token 是否过期
     *
     * @param token jwt token
     * @return true or false
     */
    public static boolean isTokenExpired(String token) {
        return parseToken(token).getExpiration().before(new Date());
    }
}

