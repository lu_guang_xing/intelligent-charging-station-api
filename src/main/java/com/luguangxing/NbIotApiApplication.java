package com.luguangxing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication()
public class NbIotApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(NbIotApiApplication.class, args);
    }
}