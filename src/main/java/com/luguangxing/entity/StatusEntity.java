package com.luguangxing.entity;

import lombok.*;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 2022/11/18
 * @desc:
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class StatusEntity implements Serializable {
    String status;
    String code;
    String msg;
    String token;
    Object data;

    public StatusEntity(String status, String code) {
        this.status = status;
        this.code = code;
    }
}
