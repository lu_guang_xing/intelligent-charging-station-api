package com.luguangxing.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@AllArgsConstructor
@Setter
@Getter
@ToString
public class ChargeLog implements Serializable {
    private int logNum;
    private int deviceNum;
    private String userCardId;
    private int interfaceNum;
    private String startDate;
    private String endDate;
    private float cost;
}
