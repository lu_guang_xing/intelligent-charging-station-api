package com.luguangxing.entity;

import lombok.*;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 2022/11/20
 * @desc:
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class LoginEntity implements Serializable {
    private StatusEntity status;
    private UserEntity user;
}
