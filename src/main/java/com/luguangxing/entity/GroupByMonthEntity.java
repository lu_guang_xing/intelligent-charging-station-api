package com.luguangxing.entity;

import lombok.*;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 2022/11/22
 * @desc:
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class GroupByMonthEntity implements Serializable {
    String month;
    String cost;
}
