package com.luguangxing.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 2023/2/20
 * @desc:
 */
@AllArgsConstructor
@Setter
@Getter
@ToString
public class CommandResponse implements Serializable {
    private String timestamp;
    private String tenantId;
    private int taskId;
    private String resultDetail;
    private String resultCode;
    private String protocol;
    private String productId;
    private String messageType;
    private String deviceId;
}
