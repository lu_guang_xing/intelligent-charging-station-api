package com.luguangxing.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 2023/2/20
 * @desc:
 */
@AllArgsConstructor
@Setter
@Getter
@ToString
public class UploadedData implements Serializable {
    private int upPacketSN;
    private int upDataSN;
    private String topic;
    private String timestamp;
    private String tenantId;
    private int serviceId;
    private String protocol;
    private String productId;
    private String payload;
    private String messageType;
    private String deviceType;
    private String deviceId;
    private String assocAssetId;
    private String IMSI;
    private String IMEI;
}
