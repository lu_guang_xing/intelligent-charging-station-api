package com.luguangxing.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 2023/2/20
 * @desc:
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ProductEntity implements Serializable {
    private String productId;
    private String productName;
    private String tenantId;
    private String productDesc;
    private String productType;
    private String secondaryType;
    private String thirdType;
    private String productProtocol;
    private String authType;
    private String payloadFormat;
    private String createTime;
    private String updateTime;
    private String networkType;
    private String endpointFormat;
    private String powerModel;
    private String apiKey;
    private String deviceCount;
    private String productTypeValue;
    private String secondaryTypeValue;
    private String thirdTypeValue;
    private String encryptionType;
    private String rootCert;
    private String createBy;
    private String updateBy;
    private String tupDeviceModel;
    private String tupDeviceType;
    private String accessType;
    private String nodeType;
    private String tupIsThrough;
    private String dataEncryption;
    private String lwm2mEdrxTime;
    private String categoryId;
    private String categoryName;
}
