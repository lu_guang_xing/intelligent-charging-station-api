package com.luguangxing.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 20/5/2023
 * @desc:
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class StationEntity implements Serializable {
    private int deviceNum;
    private String deviceImei;
    private String deviceAddress;
    private int interfaceNum;
    private String deviceStatus;
    private float pricePerHour;
    private String deviceComment;
}
