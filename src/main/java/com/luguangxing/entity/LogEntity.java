package com.luguangxing.entity;

import lombok.*;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 2022/11/17
 * @desc:
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class LogEntity implements Serializable {
    private int LogId;
    private String userCardId;
    private float cost;
    private String logDate;
}
