package com.luguangxing.entity;

import lombok.*;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 2022/11/17
 * @desc:
 */

@AllArgsConstructor
@Setter
@Getter
@ToString
public class UserEntity implements Serializable {
    private String userAccount;
    private String userPassword;
    private String userName;
    private int userAge;
    private String userSex;
    private String userCardId;
    private float userCardBalance;
    private String userEmail;
    private String userPhone;

    public UserEntity() {
        userAccount = "";
        userPassword = "";
        userName = "";
        userAge = 0;
        userSex = "";
        userCardId = "";
        userCardBalance = 0f;
        userEmail = "";
        userPhone = "";
    }
}
